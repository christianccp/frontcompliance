import api from '../api.js'
const path = '/users'
const pathOrg = '/organizations/get/'

export default {
  getUsuario (accessToken) {
    const tokenId = accessToken
    return new Promise((resolve, reject) => {
      api.get(path + '/myData?access_token=' + tokenId)
        .then(response => {
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  },
  getDatosOrganizations (token) {
    const tokenId = token
    return new Promise((resolve, reject) => {
      api.get(pathOrg + 'module?token=' + tokenId)
        .then(response => {
          resolve(response.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  },
  updateUsuario (paramsAPI) {
    return new Promise((resolve, reject) => {
      api.put(path + '/' + paramsAPI.id, paramsAPI)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          console.log('Catch:' + error)
          reject(error)
        })
    })
  }
}
