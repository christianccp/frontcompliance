import api from '../api.js'

const path = '/mercadopagos'

export default {
  configMercado (paramsApi) {
    return new Promise((resolve, reject) => {
      let url = path + '/config/mp'
      api.post(url, paramsApi)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
