import api from '../api.js'

const path = '/PaymentPaypal'

export default {
  checkoutpaypal (paramsApi) {
    return new Promise((resolve, reject) => {
      let url = path + '/checkoutpaypal'
      api.post(url, paramsApi)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
