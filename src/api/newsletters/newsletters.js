import api from '../api.js'

const path = '/newsletters'

export default {
  suscribeNewsletters (paramsApi) {
    return new Promise((resolve, reject) => {
      let url = path
      api.post(url, paramsApi)
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
    })
  }
}
