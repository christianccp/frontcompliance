import axios from 'axios'
import {URL_BACK} from '../variableProd'

export default axios.create({
  baseURL: URL_BACK,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json'
  }
})
