import api from '../api.js'

let path = '/blogs'

export default {
  getBlogs (type) {
    return new Promise((resolve, reject) => {
      api.get(path, { params: { type } })
        .then(response => { resolve(response) })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  },
  getPost (id) {
    return new Promise((resolve, reject) => {
      api.get(path + '/get/blog', { params: { id } })
        .then(response => { resolve(response) })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }
}
