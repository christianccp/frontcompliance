import api from '../api.js'

const path = '/mail'

export default {
  sendMail (paramsApi) {
    return new Promise((resolve, reject) => {
      let url = path + '/send/mail'
      api.post(url, paramsApi)
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
    })
  }
}
