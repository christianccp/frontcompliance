import api from '../api.js'
const path = '/facturations/'

export default {
  insertData (paramsApi) {
    return new Promise((resolve, reject) => {
      api.post(path + '/', paramsApi)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          console.log('Catch:' + error)
          reject(error)
        })
    })
  }
}
