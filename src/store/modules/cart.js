import swal from 'sweetalert2'

function getProductsSession () {
  let products = sessionStorage.getItem('order')
  return products === null ? [] : JSON.parse(products)
}

const state = {
  products: [
    {
      id: 1,
      name: 'Plan profesional',
      desc: 'Sistema de gestión integral',
      price: 240000,
      pricedesc: 0.75,
      premiumTag: 'moduleAnticorruption',
      statusdesc: true,
      descModule: 'Módulo anticorrupción/ año'
    },
    {
      id: 2,
      name: 'Plan Ejecutivo',
      desc: 'Sistema de gestión integral',
      price: 120000,
      pricedesc: 0.75,
      premiumTag: 'moduleAnticorruption',
      statusdesc: true,
      descModule: 'Módulo anticorrupción/ año'
    },
    {
      id: 3,
      name: 'Política de integridad',
      desc: 'Implementación documental del Sistema Anticorrupción',
      price: 120000,
      pricedesc: 0.75,
      premiumTag: 'serviceDocMgt',
      statusdesc: true,
      descModule: 'Módulo anticorrupción/ año'
    },
    {
      id: 4,
      name: 'Plan de continuidad de negocio COVID-19',
      desc: 'Implementación por pandemia (COVID-19)',
      price: 50000,
      pricedesc: 0,
      premiumTag: 'moduleContinuidad',
      statusdesc: false,
      descModule: 'Módulo Continuidad de negocio COVID-19/ año'
    },
    {
      id: 5,
      name: 'Licencia adicional',
      desc:
        'Solicita las que necesites si tu empresa cuenta con más de 50 empleados.',
      price: 5000,
      pricedesc: 0.5,
      premiumTag: 'serviceLicenceAditional',
      statusdesc: true,
      descModule: 'Módulo anticorrupción/ año'
    },
    {
      id: 6,
      name: 'Personalización de módulo con logo de tu empresa',
      desc: 'Personaliza el sistema con tu imagen corporativa',
      price: 1500,
      pricedesc: 1,
      premiumTag: 'servicePersonalization',
      statusdesc: true,
      descModule: 'Módulo anticorrupción/ año'
    },
    {
      id: 7,
      name: 'Personalización de dominio',
      desc: '',
      price: 1500,
      pricedesc: 1,
      premiumTag: 'serviceDomain',
      statusdesc: true,
      descModule: 'Módulo anticorrupción/ año'
    },
    {
      id: 8,
      name: 'Servicio de respaldo de información',
      desc: 'Respalda la información relacionada con el módulo anticorrución',
      price: 750,
      pricedesc: 1,
      premiumTag: 'serviceInfoBackup',
      statusdesc: true,
      descModule: 'Módulo anticorrupción/ año'
    }
  ],
  cartProducts: getProductsSession(),
  showCart: false,
  iva: '',
  subtotal: '',
  total: '',
  idsPlanes: [1, 2]
}

const getters = {
  getPremiumTags (state) {
    let premium = {}
    state.cartProducts.map(prodCart => {
      let product = state.products.find(prod => prodCart.id === prod.id)
      if (product) {
        premium[product.premiumTag] = true
      }
      var hoy = new Date()
      var fLim = new Date('2020-06-30').getTime()
      var diff = fLim - hoy.getTime()
      diff = diff / (1000 * 60 * 60 * 24)
      var timeHoy = hoy.getTime() + (1000 * 60 * 60 * 24 * 365)
      var newFecha = new Date(timeHoy)
      if (product.premiumTag === 'moduleContinuidad') {
        if (diff > 0) {
          premium.vigContinuidad = '2021-06-30'
        } else {
          premium.vigContinuidad = newFecha
        }
      }
      if (product.premiumTag === 'moduleAnticorruption') {
        premium.vigAnticorruption = newFecha
      }
    })
    // console.log(JSON.stringify(premium,null,2))
    return premium
  },
  getTotal (state) {
    if (state.cartProducts.length > 1) {
      let prices = state.cartProducts.map(prod => {
        return prod.price
      })
      return prices.reduce((a, b) => a + b)
    } else if (state.cartProducts.length === 1) {
      return state.cartProducts[0].price
    } else {
      return 0
    }
  },
  descMount (state) {
    if (state.cartProducts.length > 1) {
      let prices = state.cartProducts.map(prod => {
        if (prod.statusdesc) {
          return prod.price * (1 - prod.pricedesc)
        } else {
          return prod.pricedesc
        }
      })
      return prices.reduce((a, b) => a + b)
    } else if (state.cartProducts.length === 1) {
      if (state.cartProducts[0].statusdesc) {
        return (
          state.cartProducts[0].price * (1 - state.cartProducts[0].pricedesc)
        )
      } else {
        return 0
      }
    } else {
      return 0
    }
  },
  getSubTotal (state) {
    let subtotal = 0
    let desc = 0
    var precioFinal = 0
    state.cartProducts.forEach(element => {
      if (element.statusdesc) {
        desc = element.price * (1 - element.pricedesc)
        precioFinal = element.price - desc
      } else {
        precioFinal = element.price
      }
      subtotal = subtotal + precioFinal
    })
    return subtotal
  },
  getTotalDesc (state) {
    let total = 0
    let desc = 0
    var precioFinal = 0
    state.cartProducts.forEach(element => {
      if (element.statusdesc) {
        desc = element.price * (1 - element.pricedesc)
        precioFinal = element.price - desc
      } else {
        precioFinal = element.price
      }
      total = total + precioFinal
    })
    state.subtotal = total
    state.iva = total * 0.16
    state.total = total * 1.16
    return (total + state.iva)
  },
  checkoutBool (state) {
    return state.cartProducts.length > 0
  },
  isBuyingPremium (state) {
    // Obtener producto premium
    let prodPremium = state.cartProducts.filter(product =>
      state.idsPlanes.includes(product.id)
    )
    //  agrega premium a covid, para que pueda comprarse solo
    prodPremium.push(4)
    // Comprobar que hay premium, para poder comprar servicios
    if (prodPremium.length > 0) {
      return true
    } else {
      return false
    }
  },
  productsNotInCart (state) {
    let productsNotInCart = state.products.slice()
    state.cartProducts.map(prod => {
      productsNotInCart = productsNotInCart.filter(
        myProd => myProd.id !== prod.id
      )
    })
    return productsNotInCart
  }
}

const actions = {}

const mutations = {
  SET_PRODUCTS_BY_ID (state, ids) {
    state.cartProducts = []
    ids.map(id => {
      let prod = state.products.find(product => product.id === id)
      if (prod !== undefined) {
        state.cartProducts.push(prod)
        state.cartProducts.sort(function (a, b) {
          return a.id - b.id
        })
      }
    })
    sessionStorage.setItem('order', JSON.stringify(state.cartProducts))
  },
  ADD_PRODUCT_TO_CART (state, id) {
    let prod = state.products.find(product => product.id === id)
    let exist = state.cartProducts.find(product => product.id === id)
    if (prod !== undefined && exist === undefined) {
      if (state.idsPlanes.includes(id)) {
        state.cartProducts = state.cartProducts.filter(
          producto => state.idsPlanes.includes(producto.id) === false
        )
      }
      state.cartProducts.push(prod)
      getters.getTotalDesc(state)
      state.cartProducts.sort(function (a, b) {
        return a.id - b.id
      })
    }
    sessionStorage.setItem('order', JSON.stringify(state.cartProducts))
  },
  REMOVE_PRODUCT_FROM_CART (state, id) {
    if (!state.idsPlanes.includes(id)) {
      state.cartProducts = state.cartProducts.filter(prod => prod.id !== id)
      sessionStorage.setItem('order', JSON.stringify(state.cartProducts))
      getters.getTotalDesc(state)
    } else {
      swal.fire({
        title: 'No es posible eliminar este producto de su carrito',
        text: 'Debe adquirir uno de los planes disponibles',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-info btn-fill',
        type: 'info'
      })
    }
  },
  CLEAR_CART (state) {
    state.cartProducts = []
    sessionStorage.setItem('order', JSON.stringify(state.cartProducts))
  },
  SET_VALUE_SHOW_CART (state, value) {
    state.showCart = value
  },
  RESET_CART (state) {
    state.cartProducts = []
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
