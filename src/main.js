// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import Bootstrap + Vue
import BootstrapVue from 'bootstrap-vue'
// import scroll para dar efecto
import VueScrollReveal from 'vue-scroll-reveal'
// import font awesome for vue
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHandshake, faGlobeAmericas, faBan, faFlask, faSuitcase, faBinoculars, faChartLine, faWrench, faMapSigns } from '@fortawesome/free-solid-svg-icons'
import { faNewspaper, faThumbsUp, faFolderOpen, faChartBar } from '@fortawesome/free-regular-svg-icons'
import { faFacebook, faTwitterSquare, faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import smooth scroll
import VueScrollTo from 'vue-scrollto'

// Blog
import Pace from 'pace-progress'
import deviceQueries from './plugins/device-queries'

import store from './store'

import SocialSharing from 'vue-social-sharing'

Vue.config.productionTip = false

// import globally BootstrapVue
Vue.use(BootstrapVue)

Vue.use(SocialSharing)

// Smooth Scroll
Vue.use(VueScrollTo, {
  duration: 1000
})

// configurar scroll
Vue.use(VueScrollReveal, {
  class: 'v-scroll-reveal', // A CSS class applied to elements with the v-scroll-reveal directive; useful for animation overrides.
  duration: 600,
  scale: 0.3,
  distance: '0px'
})

// register each icons font awesome vue
library.add([
  faHandshake,
  faGlobeAmericas,
  faNewspaper,
  faBan,
  faFlask,
  faThumbsUp,
  faSuitcase,
  faBinoculars,
  faChartLine,
  faFolderOpen,
  faChartBar,
  faWrench,
  faFacebook,
  faTwitterSquare,
  faLinkedin,
  faMapSigns
])
Vue.component('fa-icon', FontAwesomeIcon)

// Scroll listener
Vue.directive('scroll', {
  inserted: function (el, binding) {
    let f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  }
})

Vue.use(deviceQueries, {
  phone: 'max-width: 567px',
  tablet: 'min-width: 568px',
  mobile: 'max-width: 1024px',
  laptop: 'min-width: 1025px',
  desktop: 'min-width: 1280px',
  monitor: 'min-width: 1448px'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
  mounted () {
    Pace.start()
    Pace.on('hide', () => {
      document.dispatchEvent(new Event('app.rendered'))
    })
  }
}).$mount('#app')
