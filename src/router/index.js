import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/MainPage'
import {URL_FRONT_DASH, URL_FACEBOOK, URL_LINKEDIN, URL_TWITTER} from '../variableProd'

const About = () => import(/* webpackChunkName: "about" */'@/components/About')
const Terminos = () => import(/* webpackChunkName: "terms" */'@/components/Terminos')
const AvisoPrivacidad = () => import(/* webpackChunkName: "aviso" */'@/components/AvisoPrivacidad')
const Blog = () => import(/* webpackChunkName: "blog" */'@/components/Blog')
const buyPricing = () => import(/* webpackChunkName: "Pricing" */'@/components/BuyPricing')
const Buying = () => import(/* webpackChunkName: "buying" */'@/components/Buying')
const Pay = () => import(/* webpackChunkName: "buying" */'@/components/PaymentMethods')

Vue.use(Router)
let endpointBack = URL_FRONT_DASH
export default new Router({
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return window.scrollTo({
        top: document.querySelector(to.hash).offsetTop,
        behavior: 'smooth'
      })
    } else {
      return { x: 0, y: 0 }
    }
  },
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage,
      alias: ['/index.html', '/index']
    },
    {
      path: '/buyPricing',
      name: 'buyPricing',
      component: buyPricing
    },
    {
      path: '/PaymentMethods',
      name: 'PaymentMethods',
      component: Pay
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/buying',
      name: 'Buying',
      component: Buying
    },
    // direccion de buying para captar compra de modulos
    {
      path: '/buying/:token',
      name: 'BuyingToken',
      props: true,
      component: Buying
    },
    {
      path: '/terminos',
      name: 'Terminos',
      component: Terminos
    },
    {
      path: '/avisoPrivacidad',
      name: 'AvisoPrivacidad',
      component: AvisoPrivacidad
    },
    {
      path: '/blog',
      name: 'Blog',
      component: Blog
    },
    {
      path: '/news',
      name: 'Noticias',
      component: Blog
    },
    {
      path: '/blog/by/:author',
      name: 'blogAuthor',
      props: true,
      component: Blog
    },
    {
      path: '/news/by/:author',
      name: 'newsAuthor',
      props: true,
      component: Blog
    },
    {
      path: '/blog/read/:post',
      name: 'post',
      props: true,
      component: Blog
    },
    {
      path: '/news/read/:post',
      name: 'news',
      props: true,
      component: Blog
    },
    {
      path: '/padLogin',
      name: 'login',
      beforeEnter (to, from, next) {
        window.location = endpointBack + '/login'
      }
    },
    {
      path: '/padRegister',
      name: 'register',
      beforeEnter (to, from, next) {
        var dataUrl = '/register?Paymenth=true&' + 'car=' + to.params.car
        window.location.href = endpointBack + dataUrl
      }
    },
    {
      path: '/padRegisterQuiz',
      name: 'registerQuiz',
      beforeEnter (to, from, next) {
        window.location.href = endpointBack + '/register?evaluate=true'
      }
    },
    {
      path: '/autoevaluation/anticorruption',
      name: 'anticorruption',
      beforeEnter (to, from, next) {
        window.location.href = endpointBack + '/autoevaluation/anticorruption/register?evaluate=true'
      }
    },
    {
      path: '/padRegisterView',
      name: 'registerView',
      beforeEnter (to, from, next) {
        window.location.href = endpointBack + '/register'
      }
    },
    {
      path: '/socialNetWorkFacebook',
      name: 'facebook',
      beforeEnter (to, from, next) {
        window.open(URL_FACEBOOK, '_blank')
      }
    },
    {
      path: '/socialNetWorkTwitter',
      name: 'twitter',
      beforeEnter (to, from, next) {
        window.open(URL_TWITTER, '_blank')
      }
    },
    {
      path: '/socialNetWorkLinkedin',
      name: 'linkedin',
      beforeEnter (to, from, next) {
        window.open(URL_LINKEDIN, '_blank')
      }
    },
    {path: '*', component: MainPage}
  ]
})
