# PAD

> Sitio web para despacho de abogados (Sistema Compliance)

## Preview

**[Ver en vivo](https://blackrockdigital.github.io/startbootstrap-creative/)**

## ¡Requisito indispensable para instalar!

Tener instalado NodeJS 6.0 o mayor. **[Descargar aquí](https://nodejs.org/en/)**

## Instalar
* Clonar el repo: `git clone https://gitlab.com/lennux23/pad.git`
* `cd client`
* Instalar via npm: `npm install pad`

## Usar

### Generar archivos para Producción con minification

Una vez instalado, ejecutar  `npm run build` para generar los archivos a montar en el servidor. Dichos archivos se generaran en la carpeta `/dist`.

### Generar archivos para Producción + reporte de análisis de paquetes

Ejecutar `npm run build --report`

### Modo desarrollo hot reload en localhost:8080

Para ejutar sitio en modo desarrollo ejecutar el comando `npm run dev`. Al finalizar en la consola  mostrara la liga para poder ver en el explorador el sitio.

## Bugs e Issues
Los issues deberan ser levantados en Mantis.

## Copyright and License

Copyright Cibercom 2018.

## Referencias
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
